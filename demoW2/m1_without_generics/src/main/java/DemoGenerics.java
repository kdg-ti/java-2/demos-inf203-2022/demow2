import java.util.ArrayList;
import java.util.List;

/**
 * Mark Goovaerts
 * 27/09/2022
 */
public class DemoGenerics {

    public static void main(String[] args) {
        List myList = new ArrayList();
        myList.add("Hello");
        myList.add(123);
        System.out.println(myList);

    }
}
