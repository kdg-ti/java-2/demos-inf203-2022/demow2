/**
 * Mark Goovaerts
 * 27/09/2022
 */
public class DemoBox {
    public static void main(String[] args) {
        /*
        Box<String> stringBox = new Box<>();
        stringBox.add("Hello");
        stringBox.add("World");
        System.out.println(stringBox);
        String str = stringBox.get(0);

        Box<Integer> integerBox = new Box<>();
        integerBox.add(10);
        integerBox.add(20);
        System.out.println(integerBox);
        Integer integer = integerBox.get(0);

        Box<Student> studentBox = new Box<>();
        studentBox.add(new Student(666, "Donald Trump", LocalDate.of(1946, 6, 14), "Washington"));
        studentBox.add(new Student(12345, "Sam Gooris", LocalDate.of(1973, 4, 10), "Brasschaat"));
        System.out.println(studentBox);
        Student student = studentBox.get(0);

        //Dit kan ook (gebruik maken van raw types), maar creeert wel een warning:
        Box generalBox = new Box();
        generalBox.add(5.5);
        generalBox.add("O my God!");
        generalBox.add(100);
        System.out.println(generalBox);
        */

        //Als de klasse NumberBox klaar is:
        /*
        NumberBox<Long> longBox = new NumberBox<>();
        longBox.add(10000000000L);
        longBox.add(20000000000L);
        System.out.println("average: " + longBox.average());

        NumberBox<Double> doubleBox = new NumberBox<>();
        doubleBox.add(0.5);
        doubleBox.add(1.0);
        System.out.println("average: " + doubleBox.average());
         */
        System.out.println("OK");
    }
}
