import model.Student;

import java.time.LocalDate;

/**
 * Mark Goovaerts
 * 27/09/2022
 */
public class DemoGenerics {
    public static void main(String args[]) {

        Integer[] intArray = {1, 2, 3, 4, 5};
        Double[] doubleArray = {1.1, 2.2, 3.3, 4.4};
        String[] strArray = {"Just", "Another", "Day"};
        Student[] studArray = {
                new Student(666, "Donald Trump", LocalDate.of(1946, 6, 14), "Washington"),
                new Student(12345, "Sam Gooris", LocalDate.of(1973, 4, 10))
        };

        Tools.printArray(intArray);
        Tools.printArray(doubleArray);
        Tools.printArray(strArray);
        Tools.printArray(studArray);
    }
}

